from flask import Flask, render_template, request, redirect, url_for, flash
from flask_mysqldb import MySQL
import yaml

app = Flask(__name__)

#Conexión con MySQL
db = yaml.load(open('db.yaml'))
app.config['MYSQL_HOST'] = db['mysql_host']
app.config['MYQL_USER'] = db['mysql_user']
app.config['MYSQL_PASSWORD'] = db['mysql_password']
app.config['MYSQL_DB'] = db['mysql_db']
mysql = MySQL(app)

#Configuraciones
app.secret_key = 'mysecretkey'

#Sección de ruta / decoradores

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/admin')
def dbadmin():
    return render_template('adminhome.html')

@app.route('/formcajero')
def formcajero():
    cur = mysql.connection.cursor()
    cur.execute('SELECT * FROM CAJERO')
    data = cur.fetchall()
    return render_template('form_add_cj.html', CAJERO = data)

@app.route('/addcajero', methods=['POST'])
def addcajero():
    if request.method == 'POST':
        first_name_cj = request.form['first_name_cj']
        edad_cj = request.form['edad_cj']
        phone_cj = request.form['phone_cj']
        salario_cj = request.form['salario_cj']
        cur = mysql.connection.cursor()
        cur.execute('INSERT INTO CAJERO (Nom_cj,edad, tel_cj, salario) VALUES (%s, %s, %s, %s)', (first_name_cj, edad_cj, phone_cj, salario_cj))
        mysql.connection.commit()
        flash('Información de cajero ha sido agregado con éxito!!')
        return redirect(url_for('formcajero'))

@app.route('/editar/<id>')
def get_cajero(id):
    cur = mysql.connection.cursor()
    cur.execute('SELECT * FROM CAJERO WHERE IDcj = %s', (id))
    data = cur.fetchall()
    return render_template('editar_cajero.html', cajero = data[0])

@app.route('/actualizar/<id>', methods = ['POST'])
def actualizar_cajero(id):
    if request.method == 'POST':
        Nom_cj = request.form['first_name_cj']
        edad = request.form['first_name_cj']
        tel_cj = request.form['phone_cj']
        salario = request.form['salario_cj']
        cur = mysql.connection.cursor()
        cur.execute("""
        UPDATE CAJERO
        SET Nom_cj = %s,
            edad = %s,
            tel_cj = %s,
            salario = %s
        WHERE IDcj = %s
        """, (Nom_cj, edad, tel_cj, salario, id))
        mysql.connection.comit()
        flash('Se actualizo de forma correecta la información del cajero')
        return redirect(url_for('formcajero'))

@app.route('/eliminar/<string:id>')
def eliminar_cajero(id):
    cur = mysql.connection.cursor()
    cur.execute('DELETE FROM CAJERO WHERE IDcj = {0}' .format(id))
    mysql.connection.commit()
    flash('Información de cajero ha sido eliminado con éxito!!')
    return redirect(url_for('formcajero'))

@app.route('/formproducto')
def formproducto():
    cur = mysql.connection.cursor()
    cur.execute('SELECT * FROM PRODUCTOS')
    data = cur.fetchall()
    return render_template('form_add_prod.html', PRODUCTOS = data)

@app.route('/addproducto', methods=['POST'])
def addproducto():
    if request.method == 'POST':
        Nom_prod = request.form['Nom_prod']
        precio = request.form['precio']
        cantidad = request.form['cantidad']
        cur = mysql.connection.cursor()
        cur.execute('INSERT INTO PRODUCTOS (Nom_prod,precio, cantidad) VALUES (%s, %s, %s)', (Nom_prod, precio, cantidad))
        mysql.connection.commit()
        flash('El producto ha sido agregado con éxito!!')
        return redirect(url_for('formproducto'))

@app.route('/info')
def info():
    return render_template('info.html')

@app.route('/formcolumna')
def formcolumna():
    return render_template('formcolumna.html')

@app.route('/addcolumna', methods=['POST'])
def addcolumna(tablausr, new_column):
    if request.method == 'POST':
        new_column = request.form['new_colum']
        cur = mysql.connection.cursor()
        cur.execute('ALTER TABLE %s add %s VARCHAR(30)', tablausr, new_column)
        mysql.connection.commit()
        flash('Se ha agregado la columna con éxito!!')
        return redirect(url_for('index'))

@app.route('/deltable')
def deltable(table_name):
    cur = mysql.connection.cursor()
    cur.execute('DROP TABLE %s', table_name)
    mysql.connection.commit()
    flash('Se ha eliminado a tabla %s', table_name)
    return redirect(url_for('dbadmin'))


if __name__ == '__main__':
    app.run(port=3000, debug=True)
